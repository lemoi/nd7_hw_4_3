'use strict';

pokemonApp.controller('BerryListCtrl', function($scope, BerriesService) {

    BerriesService.getBerries().then(function(response) {
        $scope.berries = response.data.results;
    });

});
