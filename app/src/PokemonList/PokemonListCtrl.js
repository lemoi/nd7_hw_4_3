'use strict';

pokemonApp.controller('PokemonListCtrl', function($scope, PokemonsService, BerriesService, $q) {

    $scope.listsLoaded = false;

    // PokemonsService.getPokemons().then(function(response) {
    //     $scope.pokemons = response.data.results;
    // });
    //
    // BerriesService.getBerries().then(function(response) {
    //     $scope.berries = response.data.results;
    // });

    /*PokemonsService.getPokemons().then(function(response) {
        $scope.pokemons = response.data.results;

        return BerriesService.getBerries()
    }).then(function(response) {
        $scope.berries = response.data.results;
        $scope.listsLoaded = true;

    });
    */

    $q.all([
        PokemonsService.getPokemons(),
        BerriesService.getBerries(),
    ])
    .then(function(responses) {
        $scope.pokemons = responses[0].data.results;
        $scope.berries = responses[1].data.results;
        $scope.listsLoaded = true;
    });

});
