'use strict';

pokemonApp.controller('EditPokemonCtrl', function($scope, $routeParams, PokemonsService) {

		$scope.pokemonLoaded = false;

    PokemonsService.getPokemon($routeParams['pokemonId']).then(function(response) {
        $scope.pokemon = response.data;
				$scope.pokemonLoaded = true;
    });

    $scope.editPokemon = function(myPokemon) {

        $scope.editError = false;
        $scope.editSuccess = false;

        PokemonsService.editPokemon(myPokemon).then(function successCallback(response) {

            // Окей!
            //$scope.pokemonId = response.data.objectId;
            $scope.editSuccess = true;

        }, function errorCallback(response) {

            // Не окей..
            $scope.editError = true;
        });


    }

});
